<?php namespace Pixeloution\Proxy;

ini_set( 'track_errors', 1 );

/**
 * TODO: impelment exceptions to indicate lack of network access, timeouts, 
 *       etc when a host is unreachable. Does Guzzle do this automatically?
 */
class Proxy
{
   /**
    * guzzle HTTP client
    * @var object
    */
   protected $client;

   /**
    * array of endpoint hashes
    * @var array
    */
   protected $endpoints = [];

   /**
    * takes dependencies on object creation or uses defaults
    * if none are specified
    * 
    * @param object $client         
    * @param object $requestFactory
    *
    * @return object
    */
   public function __construct(
         \Guzzle\Http\Client $client = null
     ,   \Guzzle\Stream\PhpStreamRequestFactory $streamFactory = null
   )
   {
      $this->client = $client ?: new \Guzzle\Http\Client;
      $this->streamFactory = $streamFactory ?: new \Guzzle\Stream\PhpStreamRequestFactory;
   }
  
   /**
    * registers a pattern (regex) to be handled and what domain
    * the request should be pointed to
    * 
    * @param  mixed $pattern
    * a regular expression, including delimiters, ex. /pattern/
    *       OR
    * an array of pattern/url pairs
    * 
    * @param  string $url
    * destination for the request, or null if using the array format for $pattern
    * 
    * @return void
    */
   public function register($pattern, $url = null)
   {
      if( is_array($pattern) )
      {
         foreach($pattern as $single)
         {
            $this->register($single[0], $single[1]);
         }
         return;
      }
      else
      {
         $this->validateExpression( $pattern );

         array_push( $this->endpoints, [ 'pattern' => $pattern, 'target' => $url] );
      }
   }
   
   /**
    * takes the request URI and determines the matching route defintion, 
    * then return the resource (data)
    * 
    * @param  [type] $requestURI [description]
    * 
    * @return bool
    * true if a file was fetched and output, else false
    */
   public function dispatch($requestURI)
   {
      $path = null;

      foreach( $this->endpoints as $endpoint )
      {
         if( preg_match($endpoint['pattern'], $requestURI, $matches) )
         {
            // use the entire path as the target path unless there's a value in
            // $matches[1] from capture parens, ie ^something/(real/route/here.ext)$
            $path = isset($matches[1]) ? $matches[1] : $matches[0];
            break;
         }
      }

      // if no path could be determined, then we should return false to indicate
      return $path ? $this->fetch( $endpoint['target'], $path ) : false;

   }

   public function getEndpoints()
   {
      return $this->endpoints;
   }

   /**
    * retrieves and streams data
    * 
    * @param  string $target
    * the base URL of the target site
    * 
    * @param  string $path
    * the path to the resource, ie. assets/js/file.js
    * 
    * @return output
    */
   protected function fetch($target, $path)
   {
      $request = $this->client->get( "$target/" . ltrim($path, '/') );      
      
      try 
      {
         $stream  = $this->streamFactory->fromRequest( $request );
      }
      catch ( \Guzzle\Http\Exception\BadResponseException $e )
      {
         switch( $e->getResponse()->getStatusCode() )
         {
            case 404:
               ErrorPage::Response(
                  '404'
               ,  'Not Found'
               ,  'Destination server reports file not found. ' . "@ $target/$path"
               );
               break;
            case 500:
               ErrorPage::Response(
                  '500'
               ,  'Internal Server Error'
               ,  'Destination server reports Internal Server Error'
               );
               break;
            default:
               ErrorPage::Response(
                  $e->getResponse()->getStatusCode()
               ,  'ERROR'
               ,  'Something Terrible Has Occoured.'
               );
               break;
         }

         return true;
      }

      foreach( $stream->getMetaData()['wrapper_data'] as $header )
      {
         header( $header );
      }

      // Read until the stream is closed
      while (!$stream->feof()) 
      {
         echo $stream->readLine();
      }

      # this method always 'succeeds', ie. it attempted to access the network and
      # get the file
      return true;
   }

   protected function validateExpression($expressionCandidate)
   {
      // checking for errors after this function call as a way to determine
      // if the regEx was valid or not -- doesn't matter what it does/doesn't match
      @preg_match( $expressionCandidate, 'unimportant' );

      if(isset($php_errormsg))
         throw new InvalidRegExException();
   }
}

class InvalidRegExException extends \Exception {}