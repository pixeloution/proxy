<?php

class ProxyTest extends PHPUnit_Framework_TestCase
{
   /**
    * runs before any of the test cases are called
    */
   public function setUp()
   {
      $this->client = $this->getMock('\Guzzle\Http\Client');
      $this->streamFactory = $this->getMock('\Guzzle\Stream\PhpStreamRequestFactory');
   }

   public function testInstantiateClass()
   {
      $this->assertTrue( (bool) $this->getProxyObject() );
   }

   /**
    * register() should not allow an invalid regular expression as the
    * first component to register
    * 
    * @expectedException \Pixeloution\Proxy\InvalidRegExException
    */
   public function testExceptionOnInvalidRegexp()
   {
      $this->getProxyObject()->register( '/invalid', 'http://www.example.com' );
   }


   public function testSingleRegistrations()
   {
      $expected = [
         [ 'pattern' => '% mypath/\d+/(.*) %xi', 'target' => 'http://example.com' ]
      ,  [ 'pattern' => '% other/(.*) %xi', 'target'=> 'http://www.other.com' ]
      ];

      $proxy = $this->getProxyObject();
      $proxy->register( '% mypath/\d+/(.*) %xi', 'http://example.com' );
      $proxy->register( '% other/(.*) %xi', 'http://www.other.com' );

      $this->assertEquals($expected, $proxy->getEndpoints());
   }

   /**
    * should be able to call register() with an array of arrays
    */
   public function testArrayRegistration()
   {
      $expected = [
         [ 'pattern' => '% mypath/\d+/(.*) %xi', 'target' => 'http://example.com' ]
      ,  [ 'pattern' => '% other/(.*) %xi', 'target'=> 'http://www.other.com' ]
      ];

      $proxy = $this->getProxyObject();
      $proxy->register([
         [ '% mypath/\d+/(.*) %xi', 'http://example.com' ]
      ,  [ '% other/(.*) %xi', 'http://www.other.com' ]
      ]);

      $this->assertEquals($expected, $proxy->getEndpoints());
   }

   protected function getProxyObject()
   {
      return new \Pixeloution\Proxy\Proxy( $this->client, $this->streamFactory );
   }
}